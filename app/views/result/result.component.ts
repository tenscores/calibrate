import { Component, ViewEncapsulation } from '@angular/core';
import { Router }      from '@angular/router';
import { Calibrate }   from '../../services/calibrate/calibrate.service';
import { Utils }       from '../../services/utils/utils.service';
import { Broadcaster } from '../../services/broadcaster/broadcaster.service';

declare var d3: any;

@Component({
  moduleId     : module.id,
  selector     : 'result',
  templateUrl  : 'templates/result.html',
  encapsulation: ViewEncapsulation.None
})

export class ResultComponent {
  conversionRate    = 0;
  maxCPA            = 0;
  valuePerClick     = '0.00';
  closest           = '';
  currency          = {};
  bid               = false;
  showBidDetails    = false;
  hasClosest: any   = null;
  calibrateData     = new Array();
  chartOptions: any = {};

  constructor(private calibrate:   Calibrate,
              private router:      Router,
              private utils:       Utils,
              private broadcaster: Broadcaster) {
    this.refresh();
    this.broadcaster.broadcast("showVideoToggle");
  }

  refresh(): void {
    this.conversionRate = this.calibrate.getConversionrate();
    this.maxCPA         = this.calibrate.getCPA();
    this.currency       = this.calibrate.getCurrency();
    this.valuePerClick  = this.calibrate.getValuePerClick().toFixed(2);
    this.bid            = this.calibrate.getBid();

    if (this.bid) {
      this.showBidDetails = true;
      this.calibrateData  = this.calibrate.calibrate();
      this.closest        = this.calibrate.getClosest();
      this.hasClosest     = this.calibrate.hasClosest();
    }

    /* Chart Data */
    let maxCPCs = new Array();
    maxCPCs.push('max_cpc');
    let profit          = new Array();
    profit.push('profit');
    /* Create copy of calibrate data */
    let calibrateData = this.calibrateData.concat([]);

    for (let i = calibrateData.length - 1; i >= 0; i--) {
      let item: any = calibrateData[i];

      if (item.max_cpc !== 'current') {
        let currency = this.calibrate.getCurrency().CurrencySymbol;
        maxCPCs.push(currency + item.max_cpc_num.toFixed(2));
      } else {
        maxCPCs.push(item.max_cpc);
      }

      profit.push(item.profit);
    }

    this.chartOptions = {
      bindto: '#chart',
      data: {
          x: 'max_cpc',
          columns: [
             maxCPCs,
             profit
          ],
          names: {
              'max_cpc': 'Bids',
              'profit': 'Profit'
          },
          type: 'spline'
      },
      legend: {
          show: false,
          position: 'bottom'
      },
      tooltip: {
          format: {
            value: function (value: any, ratio: any, id: any) {
              let format = d3.format(',');
              return this.calibrate.getCurrency().CurrencySymbol
                + format(value);
            }.bind(this)
          }
      },
      axis: {
          x: {
              label: {
                  text: 'Bids',
                  position: 'outer-left'
              },
              type: 'category'
          }
      },
      grid: {
          y: {
              show: false
          }
      }
    };
    this.broadcaster.broadcast('refreshChart', this.chartOptions);
  }

  replaceNaNICC(item: any): any {
    return isNaN(item) || !isFinite(item) ? '-' : item;
  }

  onTryTenScores(): void {
    window.open("http://tenscores.com", "_blank");
  }

  onStartAgain(): void {
    this.router.navigate(['/bid-simulator']);
  }

  updateMaxCPA(data: any): void {
    this.calibrate.setCPA(data);
    this.refresh();
  }

  updateConversionRate(data: any): void {
    this.calibrate.setConversionRate(data);
    this.refresh();
  }
}
