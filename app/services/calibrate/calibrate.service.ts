import { Injectable } from '@angular/core';
import { Utils }      from '../utils/utils.service';
import { Currency }   from '../currency/currency.service';

declare let d3: any;

export interface CalibrateData {
  conversionRate: number;
  cpa: number;
  bid: boolean;
  rawData: any;
  parsedData: any;
  currency: any;
  closest: any;
  hasClosest: boolean;
  overrideCurrency: any;
}

@Injectable()
export class Calibrate {
  private data: CalibrateData = {
    conversionRate: 0,
    cpa: 0,
    bid: false,
    rawData: '',
    currency: false,
    parsedData: new Array,
    closest: '',
    hasClosest: false,
    overrideCurrency: false
  };

  constructor(private utils:    Utils,
              private currency: Currency) { }

  setConversionRate(conversionRate: number): void {
    this.data.conversionRate = conversionRate;
  }

  setCPA(cpa: number): void {
    this.data.cpa = cpa;
  }

  setBid(bid: boolean): void {
    this.data.bid = bid;
  }

  setCurrency(currency: any): void {
    this.data.currency = currency;
  }

  setRawData(rawData: string): void {
    this.data.rawData = rawData;
  }

  setClosest(closest: any): void {
    if (closest) {
      closest.max_cpc_stripped = this.utils.getNumber(closest.max_cpc);
    }

    this.data.closest = closest;
    this.data.hasClosest = true;
  }

  setOverrideCurrency(currency: any): void {
    this.data.overrideCurrency = currency;
  }

  setAll(calibrateData: CalibrateData): void {
    this.data = calibrateData;
  }

  getConversionrate(): number {
    return this.data.conversionRate;
  }

  getCPA(): number {
    return this.data.cpa;
  }

  getBid(): boolean {
    return this.data.bid;
  }

  getCurrency(): any {
    let currency: any = this.currency.getDefault();

    if (this.data.currency) {
      currency = this.data.currency;
    }

    if (this.getOverrideCurrency()) {
      currency.CurrencySymbol = this.getOverrideCurrency();
    }

    return currency;
  }

  getRawData(): string {
    return this.data.rawData;
  }

  getAll(): CalibrateData {
    return this.data;
  }

  getValuePerClick(): number {
    return this.data.cpa * (this.data.conversionRate / 100);
  }

  getClosest(): any {
    return this.data.closest;
  }

  getOverrideCurrency(): any {
    return this.data.overrideCurrency;
  }

  hasClosest(): boolean {
    return this.data.hasClosest;
  }

  parseRawData(): any {
    if (this.data.rawData.length === 0) {
      return false;
    } else {
      let result        = new Array();
      let invalidFormat = false;
      let currency      = '';
      /* Split all rows by line breaks */
      let lineBreakSplit = this.data.rawData.split('\n');
      /* Then loop through each record */
      let self = this;
      lineBreakSplit.forEach(function (item: any) {
        let hasNumbers = /[0-9]/g;
        /* Then check if its a valid row data */
        if (hasNumbers.test(item)) {
          /* If it does split row data by tabs */
          let rowData = item.split('\t');
          if (rowData.length <= 1) {
            invalidFormat = true;
            return false;
          } else {
            /* Then put parsed data to result set */
            let obj = {
              max_cpc         : rowData[0],
              max_cpc_num     : self.utils.toNumber(rowData[0]),
              estimated_clicks: rowData[1],
              estimated_costs : rowData[2],
              currency_symbol : self.utils.getCurrencySymbol(rowData[2])
            };

            currency = obj.currency_symbol;
            result.push(obj);
          }

          self.setOverrideCurrency(currency);
        }
      });

      if (invalidFormat || result.length === 0) {
        return false;
      } else {
        return this.data.parsedData = result;
      }
    }
  }

  calibrate(): any {
    let data = this.data.parsedData;

    if (this.data.parsedData.length === 0) {
      return data;
    } else {
      let self          = this;
      let hasNaNICC     = false;
      let getICC        = function (max: any, min: any) {
        let noIC   =   self.utils.toNumber(max.estimated_clicks)
                     - self.utils.toNumber(min.estimated_clicks);
        let noCIC  =   self.utils.toNumber(max.estimated_costs)
                     - self.utils.toNumber(min.estimated_costs);
        return noCIC / noIC;
      };
      let getRevenue     = function (vpc: number, current: any) {
        return self.utils.toNumber(current.estimated_clicks) * vpc;
      };
      let getProfit      = function (current: any) {
        return   self.utils.toNumber(current.revenue)
               - self.utils.toNumber(current.estimated_costs);
      };
      let getClosest = function () {
        let closest: number = 0;

        for (let i = 1; i < data.length; i++) {
          if (   self.utils.toNumber(data[closest].profit)
              <  self.utils.toNumber(data[i].profit)) {
            hasClosest = true;
            closest = i;
          } else if (   self.utils.toNumber(data[closest].profit)
                     == self.utils.toNumber(data[i].profit)) {
            if (  data[closest].max_cpc_num
                > data[i].max_cpc_num) {
              closest = i;
            }
          }
        }

        return closest;
      };
      let checkAllICCIfGreaterThanVPC = function () {
        let checkArr: any = [];
        let counter:  any = [];

        for (let i = 0; i < data.length; i++) {
          let icc: any = self.utils.toNumber(data[i].icc);

          if (   !isNaN(icc)
              && icc > self.getValuePerClick()) {
            checkArr.push(i);
          }

          if (!isNaN(icc)) {
            counter.push(i);
          }
        }

        return checkArr.length == counter.length;
      };

      /* Remove current row */
      let currentRowIndex = -1;
      currentRowIndex = data.findIndex(function (item: any) {
        if (item.max_cpc === 'current') {
          return item;
        }
      });

      if (currentRowIndex !== -1) {
        data.splice(currentRowIndex, 1);
      }

      let format = d3.format(',.2f');
      let i      = 0;

      for (i = data.length - 1; i >= 0; i--) {
        let icc: any = '';

        if (i !== data.length - 1) {
          icc = getICC(data[i], data[i + 1]);

          if (isNaN(icc)) {
            hasNaNICC = true;
          }
          
          icc = icc.toFixed(2);
        }

        data[i].icc     = icc;
        data[i].revenue = getRevenue(this.getValuePerClick(),
                                     data[i]).toFixed(2);
        data[i].profit  = getProfit(data[i]).toFixed(2);
        data[i].closest = false;
        data[i].profit_formatted  = format(data[i].profit);
        data[i].revenue_formatted = format(data[i].revenue);
      }

      /* Fill ICC of the last row */
      let beforeLastItem = data.length - 2;
      let lastItem       = data.length - 1;
      let sumSlope       = 0;

      for (i = 0; i < beforeLastItem; i++) {
        sumSlope +=   (  this.utils.toNumber(data[i].icc)
                       - this.utils.toNumber(data[i + 1].icc))
                    / (data[i].max_cpc_num - data[i + 1].max_cpc_num);
      }

      let slope        = sumSlope / beforeLastItem;
      let sumIntercept = 0;

      for (i = 0; i < lastItem; i++) {
        // b = y - ax
        sumIntercept += this.utils.toNumber(data[i].icc) - slope * data[i].max_cpc_num;
      }

      let intercept = sumIntercept / lastItem;
      // y = ax + b
      data[lastItem].icc = (slope * data[lastItem].max_cpc_num + intercept)
        .toFixed(2);

      /* Get closest ICC */
      let closest    = getClosest();
      let hasClosest: boolean
        = (   closest == 0
           && this.utils.toNumber(data[closest].profit) <= 0
             ? false : true);

      /* Means it will never have accurate reading of closest
         due to some items contains NaN ICC
       */
      if (hasNaNICC) {
        /* Check if all ICC's are greater than VPC */
        let highestProfit: any = 0;
        closest = highestProfit;

        for (i = 1; i < data.length; i++) {
          let profitX:   any = this.utils.toNumber(data[i].profit);
          let maxCPCX:   any = data[i].max_cpc_num;
          let profitMax: any
            = this.utils.toNumber(data[highestProfit].profit);
          let maxCPCMax: any = data[highestProfit].max_cpc_num;

          if (   (profitX > profitMax)
              || (   profitX == profitMax
                  && maxCPCX < maxCPCMax)) {
            closest = i;
          }
        }

        /* Check if the highest possible profit is less than 0 */
        if (this.utils.toNumber(data[closest].profit) < 0) {
          hasClosest = false;
        } else {
          hasClosest = true;
        }
      } else if (data[closest].profit <= 0) {
        hasClosest = false;
      }

      if (hasClosest) {
      /* Algo above found the closest */
        data[closest].closest = true;
        this.setClosest(data[closest]);
      } else if (!hasClosest) {
      /* Means all ICCs are greater than VPC */
        this.setClosest("");
        this.data.hasClosest = false;
      }

      return data;
    }
  }
}
