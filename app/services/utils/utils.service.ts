import { Injectable } from '@angular/core';

@Injectable()
export class Utils {

  getNumber(numberString: string): string {
    return numberString.replace(/[^-?0-9.]/g, '');
  }

  getCurrencySymbol(numberString: string): string {
    return numberString.replace(/[-,?0-9.]/g, '');
  }

  toNumber(numberString: string): any {
    if (typeof numberString !== 'string') {
      return numberString;
    }

    return parseFloat(numberString.replace(/[^-?0-9.]/g, ''));
  }

  arrayPrepend(array: any[], prependItem: any): any[] {
    let merged: any[] = new Array();
    merged.push(prependItem);
    return merged.concat(array);
  }

  objectMerge(obj1: any, obj2: any): any {
    let merged = {};

    for (let key in obj1) {
      merged[key] = obj1[key];
    }

    for (let key in obj2) {
      merged[key] = obj2[key];
    }

    return merged;
  }
}
