import { Component } from '@angular/core';
import { OverlayInterface }
  from '../../services/overlay/overlay.service';
import { Broadcaster }
  from '../../services/broadcaster/broadcaster.service';


@Component({
  selector: 'my-app',
  templateUrl: 'templates/app.html'
})

export class AppComponent {
  showToggle: boolean = false;

  constructor(private overlay:     OverlayInterface,
              private broadcaster: Broadcaster) {
  }

  showVideo(): void {
    this.overlay.show("overlay-video");
  }

  ngOnInit() {

    this.broadcaster.on<string>("showVideoToggle").subscribe(() => {
      this.showToggle = true;
    });

    this.broadcaster.on<string>("hideVideoToggle").subscribe(() => {
      this.showToggle = false;
    });
  }
}
