var express = require('express');
var virtualDirPath = process.env.virtualDirPath || '';
var app = express();
var server = require('http').Server(app);
var path = require('path');
var bodyParser = require('body-parser');
var port = process.env.PORT || 1337;

app.set('base', '/');
app.use(express.static(__dirname + '/'));
app.use(express.static(__dirname + '/js'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(redirectUnmatched);

function redirectUnmatched(req, res) {
  res.redirect('/');
}
//Use default templating engine
app.set('view engine', 'ejs');
app.engine('.html', require('ejs').renderFile);

//Do something when you're landing on the first page
app.get('/', function(req, res) {
  res.render('index.html', {virtualDirPath: virtualDirPath});
});

server.listen(port, function(){
  console.log("Server connected. Listening on port: " + port);
});

app.use( express.static(path.join(virtualDirPath, 'public')) );
