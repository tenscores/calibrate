import { Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { Router }    from '@angular/router';
import { Calibrate } from '../../services/calibrate/calibrate.service';
import { Currency }  from '../../services/currency/currency.service';
import { OverlayInterface }
  from '../../services/overlay/overlay.service';
import { Broadcaster }
  from '../../services/broadcaster/broadcaster.service';

@Component({
  moduleId     : module.id,
  selector     : 'cpa',
  templateUrl  : 'templates/cpa.html',
  encapsulation: ViewEncapsulation.None
})

export class CPAComponent {
  cpa:        any = 0;
  selected:   any = '';
  currencies: any = new Array;

  constructor(private calibrate:   Calibrate,
              private router:      Router,
              private currency:    Currency,
              private elem:        ElementRef,
              private overlay:     OverlayInterface,
              private broadcaster: Broadcaster) {
    this.cpa        = calibrate.getCPA();
    this.currencies = currency.getCurrencyData();

    if (!calibrate.getCurrency()) {
      this.selected = currency.getDefault();
    } else {
      this.selected = calibrate.getCurrency();
    }

    this.broadcaster.broadcast("showVideoToggle");
  }

  cpaInputFocus(event: any): void {

    if (this.cpa == 0) {
      event.target.value = "";
    }
  }

  cpaInputBlur(event: any): void {

    if (   this.cpa === undefined
        || this.cpa === null
        || this.cpa == "") {
      this.cpa = 0;
    }

    if (this.cpa == 0) {
      event.target.value = "0";
    }
  }

  onHelpClicked(): void {
    this.overlay.show("");
  }

  save(): void {
    this.calibrate.setCPA(this.cpa);
    this.calibrate.setCurrency(this.selected);
  }

  onGoBack(): void {
    this.save();
    this.router.navigate(['/conversion']);
  }

  onSelectCurrency(item: any) {
    this.selected = item;
  }

  onSubmit(): void {
    this.save();
    this.router.navigate(['bid-simulator']);
  }

  ngOnInit() {
    let self = this;
    setTimeout(function () {
      let container = self.elem.nativeElement;
      let elem      = container.querySelector('.cpa');
      elem.classList.add('active');
    }, 200);
  }
}
