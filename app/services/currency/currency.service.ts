import { Injectable } from '@angular/core';

@Injectable()
export class Currency {
  currencyData = [
    {
      'CurrencyCode': 'AED',
      'CountryCode': 'ae',
      'CurrencyName': 'United Arab Emirates Dirham',
      'CurrencySymbol': 'AED.'
    },
    {
      'CurrencyCode': 'ARS',
      'CountryCode': 'sa',
      'CurrencyName': 'Argentine Peso',
      'CurrencySymbol': 'AR$'
    },
    {
      'CurrencyCode': 'AUD',
      'CountryCode': 'au',
      'CurrencyName': 'Australian Dollars',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'BGN',
      'CountryCode': 'bg',
      'CurrencyName': 'Bulgarian Lev',
      'CurrencySymbol': 'BGN.'
    },
    {
      'CurrencyCode': 'BND',
      'CountryCode': 'bn',
      'CurrencyName': 'Brunei Dollar',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'BOB',
      'CountryCode': 'bo',
      'CurrencyName': 'Bolivian Boliviano',
      'CurrencySymbol': 'BOB.'
    },
    {
      'CurrencyCode': 'BRL',
      'CountryCode': 'br',
      'CurrencyName': 'Brazilian Real',
      'CurrencySymbol': 'R$'
    },
    {
      'CurrencyCode': 'CAD',
      'CountryCode': 'ca',
      'CurrencyName': 'Canadian Dollars',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'CHF',
      'CountryCode': 'xh',
      'CurrencyName': 'Swiss Francs',
      'CurrencySymbol': 'CHF.'
    },
    {
      'CurrencyCode': 'CLP',
      'CountryCode': 'cl',
      'CurrencyName': 'Chilean Peso',
      'CurrencySymbol': 'CL$'
    },
    {
      'CurrencyCode': 'CNY',
      'CountryCode': 'cn',
      'CurrencyName': 'Yuan Renminbi',
      'CurrencySymbol': '¥'
    },
    {
      'CurrencyCode': 'COP',
      'CountryCode': 'co',
      'CurrencyName': 'Colombian Peso',
      'CurrencySymbol': 'COP.'
    },
    {
      'CurrencyCode': 'CSD',
      'CountryCode': 'rs',
      'CurrencyName': 'Old Serbian Dinar',
      'CurrencySymbol': 'CSD.'
    },
    {
      'CurrencyCode': 'CZK',
      'CountryCode': 'cz',
      'CurrencyName': 'Czech Koruna',
      'CurrencySymbol': 'CZK.'
    },
    {
      'CurrencyCode': 'DKK',
      'CountryCode': 'dk',
      'CurrencyName': 'Denmark Kroner',
      'CurrencySymbol': 'DKK.'
    },
    {
      'CurrencyCode': 'EGP',
      'CountryCode': 'eg',
      'CurrencyName': 'Egyptian Pound',
      'CurrencySymbol': 'EGP.'
    },
    {
      'CurrencyCode': 'EUR',
      'CountryCode': 'fr',
      'CurrencyName': 'Euros',
      'CurrencySymbol': '€'
    },
    {
      'CurrencyCode': 'FJD',
      'CountryCode': 'fj',
      'CurrencyName': 'Fiji Dollar',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'GBP',
      'CountryCode': 'gb',
      'CurrencyName': 'British Pounds Sterling',
      'CurrencySymbol': '£'
    },
    {
      'CurrencyCode': 'HKD',
      'CountryCode': 'cn',
      'CurrencyName': 'Hong Kong Dollars',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'HRK',
      'CountryCode': 'hr',
      'CurrencyName': 'Croatian Kuna',
      'CurrencySymbol': 'HRK.'
    },
    {
      'CurrencyCode': 'HUF',
      'CountryCode': 'hu',
      'CurrencyName': 'hu Forint',
      'CurrencySymbol': 'HUF.'
    },
    {
      'CurrencyCode': 'IDR',
      'CountryCode': 'id',
      'CurrencyName': 'Indonesian Rupiah',
      'CurrencySymbol': 'IDR.'
    },
    {
      'CurrencyCode': 'ILS',
      'CountryCode': 'il',
      'CurrencyName': 'Israeli Shekel',
      'CurrencySymbol': '₪'
    },
    {
      'CurrencyCode': 'INR',
      'CountryCode': 'in',
      'CurrencyName': 'Indian Rupee',
      'CurrencySymbol': 'INR.'
    },
    {
      'CurrencyCode': 'JPY',
      'CountryCode': 'jp',
      'CurrencyName': 'Japanese Yen',
      'CurrencySymbol': '¥'
    },
    {
      'CurrencyCode': 'KES',
      'CountryCode': 'ke',
      'CurrencyName': 'Kenyan Shilling',
      'CurrencySymbol': 'KES.'
    },
    {
      'CurrencyCode': 'KRW',
      'CountryCode': 'kr',
      'CurrencyName': 'South Korean Won',
      'CurrencySymbol': '₩'
    },
    {
      'CurrencyCode': 'MAD',
      'CountryCode': 'ma',
      'CurrencyName': 'Moroccan Dirham',
      'CurrencySymbol': 'MAD.'
    },
    {
      'CurrencyCode': 'MXN',
      'CountryCode': 'mx',
      'CurrencyName': 'Mexico Peso',
      'CurrencySymbol': 'MXN.'
    },
    {
      'CurrencyCode': 'MYR',
      'CountryCode': 'my',
      'CurrencyName': 'Malaysian Ringgit',
      'CurrencySymbol': 'MYR.'
    },
    {
      'CurrencyCode': 'NOK',
      'CountryCode': 'no',
      'CurrencyName': 'Norway Kroner',
      'CurrencySymbol': 'NOK.'
    },
    {
      'CurrencyCode': 'NZD',
      'CountryCode': 'nz',
      'CurrencyName': 'New Zealand Dollars',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'PEN',
      'CountryCode': 'pe',
      'CurrencyName': 'Peruvian Nuevo Sol',
      'CurrencySymbol': 'PEN.'
    },
    {
      'CurrencyCode': 'PHP',
      'CountryCode': 'ph',
      'CurrencyName': 'Philippine Peso',
      'CurrencySymbol': 'PHP.'
    },
    {
      'CurrencyCode': 'PKR',
      'CountryCode': 'pk',
      'CurrencyName': 'Pakistan Rupee',
      'CurrencySymbol': 'PKR.'
    },
    {
      'CurrencyCode': 'PLN',
      'CountryCode': 'pl',
      'CurrencyName': 'Polish New Zloty',
      'CurrencySymbol': 'PLN.'
    },
    {
      'CurrencyCode': 'RON',
      'CountryCode': 'ro',
      'CurrencyName': 'New Romanian Leu',
      'CurrencySymbol': 'RON.'
    },
    {
      'CurrencyCode': 'RSD',
      'CountryCode': 'rs',
      'CurrencyName': 'Serbian Dinar',
      'CurrencySymbol': 'RSD.'
    },
    {
      'CurrencyCode': 'RUB',
      'CountryCode': 'ru',
      'CurrencyName': 'Russian Rouble',
      'CurrencySymbol': 'RUB.'
    },
    {
      'CurrencyCode': 'SAR',
      'CountryCode': 'sa',
      'CurrencyName': 'Saudi Riyal',
      'CurrencySymbol': 'SAR.'
    },
    {
      'CurrencyCode': 'SEK',
      'CountryCode': 'se',
      'CurrencyName': 'Sweden Kronor',
      'CurrencySymbol': 'SEK.'
    },
    {
      'CurrencyCode': 'SGD',
      'CountryCode': 'sg',
      'CurrencyName': 'Singapore Dollars',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'SIT',
      'CountryCode': 'si',
      'CurrencyName': 'Slovenian Tolar',
      'CurrencySymbol': 'SIT.'
    },
    {
      'CurrencyCode': 'SKK',
      'CountryCode': 'sk',
      'CurrencyName': 'Slovak Koruna',
      'CurrencySymbol': 'SKK.'
    },
    {
      'CurrencyCode': 'THB',
      'CountryCode': 'th',
      'CurrencyName': 'Thai Baht',
      'CurrencySymbol': '฿'
    },
    {
      'CurrencyCode': 'TRL',
      'CountryCode': 'tr',
      'CurrencyName': 'Turkish Lira',
      'CurrencySymbol': 'TRL.'
    },
    {
      'CurrencyCode': 'TRY',
      'CountryCode': 'tr',
      'CurrencyName': 'New Turkish Lira',
      'CurrencySymbol': 'TRY.'
    },
    {
      'CurrencyCode': 'TWD',
      'CountryCode': 'tw',
      'CurrencyName': 'New Taiwan Dollar',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'UAH',
      'CountryCode': 'ua',
      'CurrencyName': 'Ukrainian Hryvnia',
      'CurrencySymbol': 'UAH.'
    },
    {
      'CurrencyCode': 'USD',
      'CountryCode': 'us',
      'CurrencyName': 'US Dollars',
      'CurrencySymbol': '$'
    },
    {
      'CurrencyCode': 'VEF',
      'CountryCode': 've',
      'CurrencyName': 'Venezuela Bolivar Fuerte',
      'CurrencySymbol': 'VEF.'
    },
    {
      'CurrencyCode': 'VND',
      'CountryCode': 'vn',
      'CurrencyName': 'Vietnamese Dong',
      'CurrencySymbol': '₫'
    },
    {
      'CurrencyCode': 'ZAR',
      'CountryCode': 'za',
      'CurrencyName': 'South African Rand',
      'CurrencySymbol': 'R'
    }
  ];

  getCurrencyData(): any {
    return this.currencyData;
  }

  getDefault(): any {
    return this.currencyData[50];
  }
}
