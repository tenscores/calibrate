import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  moduleId     : module.id,
  selector     : '[bid-simulator-help-2]',
  templateUrl  : 'templates/bid-simulator-help-2.html',
  encapsulation: ViewEncapsulation.None
})

export class BidSimulatorHelp2Component {
}
