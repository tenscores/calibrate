import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  moduleId     : module.id,
  selector     : '[bid-simulator-help-1]',
  templateUrl  : 'templates/bid-simulator-help-1.html',
  encapsulation: ViewEncapsulation.None
})

export class BidSimulatorHelp1Component {
}
