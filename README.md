# Calibrate

The calibrate app is an angular node application. The angular app is served by node. The API has been developed in node

#### Install everything in one enter
    $ npm install && gulp build

#### Build and create compiled bundled version of JS / CSS files
    $ npm run build

#### Run the app (On a live server this is not needed to be executed)
    $ npm start

#### Manually compile typescript files
    $ tsc -p tsconfig.json         