import { Router }    from '@angular/router';
import { Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { Broadcaster }
  from '../../services/broadcaster/broadcaster.service';

@Component({
  moduleId     : module.id,
  selector     : 'home',
  templateUrl  : 'templates/home.html',
  encapsulation: ViewEncapsulation.None
})

export class HomeComponent {
  constructor(private router:      Router,
              private elem:        ElementRef,
              private broadcaster: Broadcaster) {
    this.broadcaster.broadcast("hideVideoToggle");
  }

  getStarted(): void {
    /* TODO: Redirect to conversion view */
    this.router.navigate(['/conversion']);
  }

  ngOnInit() {
    let self = this;
    setTimeout(function () {
      let container = self.elem.nativeElement;
      let homeElem  = container.querySelector('.home');
      homeElem.classList.add('active');
    }, 200);
  }
}
