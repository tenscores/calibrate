import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  moduleId     : module.id,
  selector     : '[cpa-help]',
  templateUrl  : 'templates/cpa-help.html',
  encapsulation: ViewEncapsulation.None
})

export class CPAHelpComponent {
}
