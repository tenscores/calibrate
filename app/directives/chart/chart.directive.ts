import { Component, ElementRef, Input, Renderer } from '@angular/core';
import { Broadcaster } from '../../services/broadcaster/broadcaster.service';

declare var c3: any;

@Component({
  moduleId   : module.id,
  selector   : '[chart]',
  template   : '<div id="chart" class="chart"></div>'
})

export class ChartDirective {
  chart: any = {};

  constructor(private el:          ElementRef,
              private renderer:    Renderer,
              private broadcaster: Broadcaster) {
    this.broadcaster.on<string>('refreshChart').subscribe(options => {
      this.chart.destroy();
      this.chart = c3.generate(options);
    });
  }

  @Input('options') opts: any;

  ngOnInit() {
    this.chart = c3.generate(this.opts);
  }
}
