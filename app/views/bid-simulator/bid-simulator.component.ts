import { Component, ElementRef,
  ViewEncapsulation } from '@angular/core';
import { Router }    from '@angular/router';
import { Calibrate }
  from '../../services/calibrate/calibrate.service';
import { OverlayInterface }
  from '../../services/overlay/overlay.service';
import { Broadcaster }
  from '../../services/broadcaster/broadcaster.service';

@Component({
  moduleId     : module.id,
  selector     : 'bid-simulator',
  templateUrl  : 'templates/bid-simulator.html',
  encapsulation: ViewEncapsulation.None
})

export class BidSimulatorComponent {
  conversionRate = 0;
  maxCPA         = 0;
  currency       = {};
  nextStep       = false;
  rawData        = '';

  constructor(private calibrate:   Calibrate,
              private router:      Router,
              private elem:        ElementRef,
              private overlay:     OverlayInterface,
              private broadcaster: Broadcaster) {
    this.conversionRate = calibrate.getConversionrate();
    this.maxCPA         = calibrate.getCPA();
    this.currency       = calibrate.getCurrency();
    this.rawData        = calibrate.getRawData();
    this.broadcaster.broadcast("showVideoToggle");
  }

  ngOnInit() {
    let self = this;
    setTimeout(function () {
      let container = self.elem.nativeElement;
      let elem      = container.querySelector('.bid-simulator');
      elem.classList.add('active');
    }, 200);
  }

  onHowToCheck(): void {
    this.overlay.show("");
  }

  seeHowClicked(): void {
    this.overlay.show("");
  }

  updateConversionRate(data: any): void {
    this.calibrate.setConversionRate(data);
  }

  updateMaxCPA(data: any): void {
    this.calibrate.setCPA(data);
  }

  onNextClicked(): void {
    this.calibrate.setRawData(this.rawData);
    if (!this.calibrate.parseRawData()) {
      alert('Pasted raw data is invalid. Please copy it directly from the Google Adwords site');
    } else {
      this.router.navigate(['/result']);
    }
  }

  onStartAgain(): void {
    this.router.navigate(['/cpa']);
  }

  onYes(): void {
    this.calibrate.setBid(true);
    this.nextStep = true;
  }

  onNo(): void {
    this.calibrate.setOverrideCurrency(false);
    this.calibrate.setBid(false);
    this.router.navigate(['/result']);
  }
}
