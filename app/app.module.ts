import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { RouterModule }  from '@angular/router';

import { AppComponent }  from './views/app/app.component';
import { PopupVideoComponent }
  from './views/app/popup-video.component';

// Modules
import { HomeComponent } from './views/home/home.component';
import { ConversionComponent }
  from './views/conversion/conversion.component';
import { ConversionHelpComponent }
  from './views/conversion/conversion-help.component';
import { CPAComponent }  from './views/cpa/cpa.component';
import { CPAHelpComponent }
  from './views/cpa/cpa-help.component';
import { ResultComponent } from './views/result/result.component';
import { BidSimulatorComponent }
  from './views/bid-simulator/bid-simulator.component';
import { BidSimulatorHelp1Component }
  from './views/bid-simulator/bid-simulator-help-1.component';
import { BidSimulatorHelp2Component }
  from './views/bid-simulator/bid-simulator-help-2.component';

// Directives
import { ChartDirective }
  from './directives/chart/chart.directive';
import { CurrencySelectDirective }
  from './directives/currency-select/currency-select.directive';
import { FloatInfoDirective }
  from './directives/float-info/float-info.directive';

// Services
import { Calibrate }   from './services/calibrate/calibrate.service';
import { Currency }    from './services/currency/currency.service';
import { Utils }       from './services/utils/utils.service';
import { Broadcaster }
  from './services/broadcaster/broadcaster.service';
import { OverlayInterface, OverlayComponent }
  from './services/overlay/overlay.service';

enableProdMode();
@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path     : '',
        component: HomeComponent
      },
      {
        path     : 'conversion',
        component: ConversionComponent
      },
      {
        path     : 'cpa',
        component: CPAComponent
      },
      {
        path     : 'bid-simulator',
        component: BidSimulatorComponent
      },
      {
        path     : 'result',
        component: ResultComponent
      },
      {
        path      : '**',
        redirectTo: '',
      }
    ])
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ConversionComponent,
    ConversionHelpComponent,
    CPAComponent,
    CPAHelpComponent,
    CurrencySelectDirective,
    FloatInfoDirective,
    ChartDirective,
    BidSimulatorComponent,
    BidSimulatorHelp1Component,
    BidSimulatorHelp2Component,
    ResultComponent,
    OverlayComponent,
    PopupVideoComponent
  ],
  providers: [
    Calibrate,
    Currency,
    Utils,
    Broadcaster,
    OverlayInterface
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
