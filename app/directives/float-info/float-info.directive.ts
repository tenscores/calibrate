import { Component, ElementRef, Input, Output, EventEmitter,
  Renderer, ViewEncapsulation } from '@angular/core';

@Component({
  moduleId   : module.id,
  selector   : '[float-info]',
  templateUrl: 'templates/float-info.html',
  encapsulation: ViewEncapsulation.None
})
export class FloatInfoDirective {
  showMaxCPAInput = false;
  showConversionRateInput = false;

  constructor(private el:       ElementRef,
              private renderer: Renderer) {
  }

  @Input('conversionRate') conversionRate: any;

  @Input('currency') currency: any;

  @Input('maxCPA') maxCPA: any;

  @Output() onMaxCPAChange = new EventEmitter();

  @Output() onConversionRateChange = new EventEmitter();

  @Output() onMaxCPAUpdate = new EventEmitter();

  @Output() onConversionRateUpdate = new EventEmitter();

  triggerInputMaxCPA(): void {
    this.showMaxCPAInput = true;
    this.onMaxCPAChange.emit();
  }

  onConversionInputKeyUp(event: any): void {

    if (   event.keyCode == 13
        || event.key == "Enter") {
      this.updateConversionRate();
    }
  }

  onMaxCPAInputKeyUp(event: any): void {

    if (   event.keyCode == 13
        || event.key == "Enter") {
      this.updateMaxCPA();
    }
  }

  updateMaxCPA(): void {
    this.showMaxCPAInput = false;
    this.onMaxCPAUpdate.emit(this.maxCPA);
  }

  triggerInputConversionRate(): void {
    this.showConversionRateInput = true;
    this.onConversionRateChange.emit();
  }

  updateConversionRate(): void {
    this.showConversionRateInput = false;
    this.onConversionRateUpdate.emit(this.conversionRate);
  }
}
