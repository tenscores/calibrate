import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  moduleId     : module.id,
  selector     : '[conversion-help]',
  templateUrl  : 'templates/conversion-help.html',
  encapsulation: ViewEncapsulation.None
})

export class ConversionHelpComponent {
}
