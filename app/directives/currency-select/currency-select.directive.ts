import { Component, ElementRef, HostListener, Input, Output, EventEmitter,
         Renderer, ViewEncapsulation } from '@angular/core';

@Component({
  moduleId     : module.id,
  selector     : '[currency-select]',
  templateUrl  : 'templates/currency-select.html',
  encapsulation: ViewEncapsulation.None,
  host         : {
    '(document:click)': 'onDocumentClicked($event)'
  }
})
export class CurrencySelectDirective {
  constructor(private el:       ElementRef,
              private renderer: Renderer) {
  }

  @Input('items') list: any;

  @Input('selectedItem') selected: any;

  @Output() onSelect = new EventEmitter();

  @HostListener('click') onHideList() {
    if (this.isListVisible()) {
      this.hideList();
    } else {
      this.showList();
    }
  }

  onDocumentClicked(event: any) {
    if (!this.el.nativeElement.contains(event.target)) {
      this.hideList();
    }
  }

  isListVisible(): boolean {
    let container = this.el.nativeElement;
    let list      = container.querySelector('.list');
    return list.classList.contains('active');
  }

  onItemClicked(item: any): void {
    this.setSelected(item);
    this.onSelect.emit(this.selected);
  }

  setSelected(item: any): void {
    this.selected = item;
  }

  showList(): void {
    let container = this.el.nativeElement;
    let list      = container.querySelector('.list');
    list.classList.add('active');
  }

  hideList(): void {
    let container = this.el.nativeElement;
    let list      = container.querySelector('.list');
    list.classList.remove('active');
  }
}
