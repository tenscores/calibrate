import { Injectable, Component, ElementRef,
  ViewEncapsulation, Input } from '@angular/core';

declare var document: any;
declare var $: any;

@Injectable()
export class OverlayInterface {
  events: any =  {};

  on(event: string, callback: any): void {
    this.events[event] = callback;
  }

  trigger(event: string, params: any): void {
    this.events[event](params);
  }

  show(id: any): void {
    this.trigger("show", id);
  }

  hide(id: any): void {
    this.trigger("hide", id);
  }
}

@Component({
  selector: 'overlay',
  templateUrl: 'templates/overlay.html',
  encapsulation: ViewEncapsulation.None
})

export class OverlayComponent {

  constructor(private elem:    ElementRef,
              private overlay: OverlayInterface) {
  }

  @Input("id") id: string = "";

  show(id?: string): void {
    let container: any = this.elem.nativeElement;

    if (id !== undefined && id != "") {
      container 
        = document.querySelector("overlay#" + id);
    }

    container.classList.add("active");
  }

  hide(id?: string): void {
    let container: any = this.elem.nativeElement;

    if (id !== undefined && id != "") {
      container 
        = document.querySelector("overlay#" + id);
    }

    container.classList.remove("active");
  }

  ngOnInit() {
    this.overlay.on("show", (params?: any) => {
      this.show(params);
    });

    this.overlay.on("hide", (params?: any) => {
      this.hide(params);
    });
  }
}
