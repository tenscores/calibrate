import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  moduleId     : module.id,
  selector     : '[popup-video]',
  templateUrl  : 'templates/popup-video.html',
  encapsulation: ViewEncapsulation.None
})

export class PopupVideoComponent {
}
