import { Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { Router}     from '@angular/router';
import { Calibrate } from '../../services/calibrate/calibrate.service';
import { OverlayInterface }
  from '../../services/overlay/overlay.service';
import { Broadcaster }
  from '../../services/broadcaster/broadcaster.service';

declare var document: any;

@Component({
  moduleId     : module.id,
  selector     : 'conversion',
  templateUrl  : 'templates/conversion.html',
  encapsulation: ViewEncapsulation.None
})

export class ConversionComponent {
  conversionRate: any = 0;

  constructor(private calibrate:   Calibrate,
              private router:      Router,
              private elem:        ElementRef,
              private overlay:     OverlayInterface,
              private broadcaster: Broadcaster) {
    this.conversionRate = calibrate.getConversionrate();
    this.broadcaster.broadcast("showVideoToggle");
  }

  onConversionInputFocus(event: any): void {

    if (this.conversionRate == 0) {
      event.target.value = "";
    }
  }

  onConversionInputBlur(event: any): void {

    if (   this.conversionRate === undefined
        || this.conversionRate === null
        || this.conversionRate == "") {
      this.conversionRate = 0;
    }

    if (this.conversionRate == 0) {
      event.target.value = "0";
    }
  }

  save(): void {
    this.calibrate.setConversionRate(this.conversionRate);
  }

  onHelpClicked(): void {
    this.overlay.show("");
  }

  onGoBack(): void {
    this.save();
    this.router.navigate(['/']);
  }

  onSubmit(): void {
    this.save();
    this.router.navigate(['/cpa']);
  }

  ngOnInit() {
    let self = this;
    setTimeout(function () {
      let container = self.elem.nativeElement;
      let elem      = container.querySelector('.conversion');
      elem.classList.add('active');
    }, 200);
  }
}
