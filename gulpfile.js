var gulp       = require('gulp'),
    tsify      = require('tsify'),
    browserify = require('browserify'),
    uglify     = require('gulp-uglify'),
    source     = require('vinyl-source-stream'),
    sass       = require('gulp-sass'),
    streamify  = require('gulp-streamify'),
    rename     = require('gulp-rename'),
    concatCSS  = require('gulp-concat-css');

gulp.task('build', ['app-bundle']);

gulp.task('move-templates', function () {
  var srcs = [
    'app/directives/float-info/templates/float-info.html',
    'app/directives/currency-select/templates/currency-select.html',
    'app/views/app/templates/app.html',
    'app/views/app/templates/popup-video.html',
    'app/views/bid-simulator/templates/bid-simulator.html',
    'app/views/bid-simulator/templates/bid-simulator-help-1.html',
    'app/views/bid-simulator/templates/bid-simulator-help-2.html',
    'app/views/conversion/templates/conversion.html',
    'app/views/conversion/templates/conversion-help.html',
    'app/views/cpa/templates/cpa.html',
    'app/views/cpa/templates/cpa-help.html',
    'app/views/home/templates/home.html',
    'app/views/result/templates/result.html',
    'app/services/overlay/templates/overlay.html'
  ];

  return gulp.src(srcs)
    .pipe(gulp.dest('templates'));
});

gulp.task('compile-sass', function () {
  return gulp.src('assets/css/style.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('merge-styles', ['compile-sass'], function () {
  var srcs = [
    'assets/css/style.css'
  ];

  return gulp.src(srcs)
    .pipe(concatCSS('style.min.css'))
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('assets/css'));
});

gulp.task('build-nots', ['move-templates', 'merge-styles']);

gulp.task('app-bundle', ['move-templates', 'merge-styles'], function () {
  return browserify()
    .add('app/main.ts')
    .plugin(tsify, { noImplicitAny: true })
    .bundle()
    .pipe(source('app/main.ts'))
    .pipe(streamify(uglify()))
    .pipe(rename('app.min.js'))
    .pipe(gulp.dest('./dist'));
});